# apollo-link-lambda

## Installation

```sh
yarn add apollo-link graphql apollo-link-lambda
```

## Usage

```js
import { InMemoryCache } from 'apollo-cache-inmemory'
import ApolloClient from 'apollo-client'
import { createLambdaTransport } from 'apollo-link-lambda'
import AWS from 'aws-sdk'

AWS.config.update({ region: 'ap-southeast-2' }) // configure AWS however you want
const lambda = new AWS.Lambda()
const functionName = 'myGraphQlLambda'

const client = new ApolloClient({
  link: createLambdaTransport(lambda, functionName),
  cache: new InMemoryCache()
})

client.query({
  query: MY_QUERY,
  variables: MY_VARIABLES
})
  .then(data => {
    console.log(data)
  })
```

Your Lambda could be implemented something like this

```js
import { buildSchema, graphql } from 'graphql'

const schema = buildSchema(`...schema...`)
const root = { /* function mappings */ }

export const handler = (event, context, callback) => {
  const { query, variables } = event
  return graphql(schema, query, root, {}, variables)
    .then(response => {
      if (response.errors) {
        throw response.errors[0]
      }
      callback(null, response)
    })
    .catch(err => {
      console.error({
        message: 'caught error',
        err
      })
      callback('An error occurred', null)
    })
}
```

## Changelog

### 0.1.0

- Initial release

## License

Released under the [Copyfree Open Innovation License](http://coil.apotheon.org/).
