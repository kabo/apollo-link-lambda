import { ApolloLink, Observable } from 'apollo-link'
import { print } from 'graphql/language/printer'
// import { inspect } from 'util'

export function createLambdaTransport(lambda, functionName, options) {
  return new ApolloLink(operation => {
    const { variables } = operation
    const query = print(operation.query)
    const params = {
      FunctionName: functionName,
      Payload: JSON.stringify({ query, variables }),
    }
    // console.log(inspect(params, false, null))
    return new Observable(observer => {
      lambda.invoke(params).promise()
        .then(({ StatusCode, Payload }) => {
          // console.log(inspect(StatusCode, false, null))
          // console.log(inspect(Payload, false, null))
          if (StatusCode !== 200) {
            // console.error('Lambda returned an error', StatusCode, Payload)
            throw new Error('An error occured')
          }
          const payload = JSON.parse(Payload)
          // console.log(inspect(payload, false, null))
          observer.next(payload)
          observer.complete()
        })
        .catch(err => {
          // console.error(inspect(err, false, null))
          observer.error(err)
        })
    })
  })
}
