const path = require('path')

module.exports = {
  devtool: 'source-map',
  entry: { index: './src/index.js' },
  externals: ['apollo-link', 'graphql/language/printer'],
  output: {
    libraryTarget: 'commonjs',
    path: path.join(__dirname, 'dist'),
    filename: '[name].js',
  },
  mode: 'production'
}

